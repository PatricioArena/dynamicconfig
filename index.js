
const express = require('express');
const Environment = require('./config/Environment');
new Environment('development');

//console.log(`global.Config: ${JSON.stringify(global.Config, undefined, global.Config.json_indentation)}`);

const app = express();

app.get('/', (req, res) => {
    res.json(global.Config);
});

app.listen(global.Config.port, () => {
    if (global.Config.port === 4200) {
        console.error(`Server running on http://localhost:${global.Config.port}`)
    }
});