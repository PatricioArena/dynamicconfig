const config = {
    "development": {
        "id": "development",
        "app_name": "my app",
        "app_desc": "my app desc",
        "port": 4200,
        "json_indentation": 4,
        "database": "my-app-db-dev"
    },
    "testing": {
        "id": "testing",
        "database": "my-app-db-test"
    },
    "staging": {
        "id": "staging",
        "port": 8080,
        "database": "my-app-db-stag"
    },
    "production": {
        "id": "production",
        "port": 8080,
        "database": "my-app-db-prod"
    }
};

class Environment {

    constructor(ambient) {
        this._setting(ambient);
    }

    _setting(param) {
        const defaultConfig = config.development;
        const environment = process.env.NODE_ENV || param;
        const environmentConfig = config[environment];
        global.Config = Object.assign(defaultConfig, environmentConfig);
    }
}

module.exports = Environment;